<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		 {
			$this->db = $db;
		 }
		  else
		  {
       try
       {
        // Create PDO connection
          $this->db = new PDO('mysql:dbname=stiansfirstdb;host=localhost','root', '');
        //array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          echo "Connected successfully. <br>";

       }
       catch (PDOException $e)
        {

        echo "Connection failed: " . $e->getMessage();
        echo "<br>";
        }
       }
     }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
    $stmt = $this->db->prepare('select * from Book');
    $stmt->execute();
    while($row = $stmt->fetchObject()){
      $booklist[]= new Book($row->Title,$row->Author,$row->Description,$row->ID);
     }
        return $booklist;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
      if(!is_numeric($id))
        return null;
		$book = null;
    $stmt = $this->db->prepare('select * from Book where id=:id');
    $stmt->execute([':id'=>$id]);
    $row= $stmt->fetchObject();
    if($row)  //Brukes for at bare eksisterende bøker kan vises.
      $book= new Book($row->Title,$row->Author,$row->Description,$row->ID);
    return $book;
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
      if(empty($book->title) || empty($book->author))
        throw new Exception("Title and Author can't be empty.");

      $stmt = $this->db->prepare('insert into book (Title,Author,Description) values(:Title,:Author,:Description)');
      $stmt->execute([':Title'=>$book->title,
      ':Author'=>$book->author,
      ':Description'=>$book->description]);
      $book->id=$this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      if(empty($book->title) || empty($book->author))
        throw new Exception("Title and Author can't be empty.");
      $stmt = $this->db->prepare('update book set Title=:Title,Author=:Author,Description=:Description where id=:id');
      $stmt->execute([':Title'=>$book->title,
      ':Author'=>$book->author,
      ':Description'=>$book->description,
      ':id'=>$book->id]);
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      if(!is_numeric($id))        //om ikke tall, gi error.
        return null;
      $stmt = $this->db->prepare('delete from Book where id=:id');
      $stmt->bindValue(':id', $id);
      $stmt->execute();
    }

}

?>
